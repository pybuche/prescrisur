import pytest
from flask import url_for

from api.models import Substance


@pytest.mark.parametrize('collection_name', ['Substance'])
def test_get_substance(mock_model, client, user):
	# Given
	obj = {"_id": "02039", "status": None, "name": "MAGNESIA", "specialities":  [], 'created_at': None, 'updated_at': None, 'deleted_at': None}
	mock_model.collection.insert(obj)

	# When
	res = client.get(url_for('api.substance', subst_id='02039'))
	data = res.json

	# Then
	assert res.status_code == 200
	assert data['data'] == obj


@pytest.mark.parametrize('collection_name', ['Substance'])
def test_get_substance_no_subst_404(mock_model, client, user):
	# When
	res = client.get(url_for('api.substance', subst_id='02039'))

	# Then
	assert res.status_code == 404


@pytest.mark.parametrize('collection_name', ['Substance'])
def test_get_substance_not_logged_in_401(mock_model, client):
	# Given
	obj = {"_id": "02039", "status": None, "name": "MAGNESIA", "specialities":  []}
	mock_model.collection.insert(obj)

	# When
	res = client.get(url_for('api.substance', subst_id='02039'))

	# Then
	assert res.status_code == 401